% Font size
\documentclass[12pt,a4paper,british]{article}

% utf8 support
\usepackage[utf8]{inputenc}

\usepackage{hyperref}

% Referencing
\usepackage[backend=biber,style=apa]{biblatex}
\addbibresource{references.bib}
% break bilbatex reference URL on number
\setcounter{biburlnumpenalty}{9000}

% packages for Header and Footer
\usepackage{lastpage}
\usepackage{fancyhdr}

% quotations
\usepackage[autostyle]{csquotes}
\MakeOuterQuote{"}

%!TeX spellcheck = en-GB

%----------------------------------------------------------------------------------------
%	TITLE SECTION
%----------------------------------------------------------------------------------------

\title{\textbf{How does China’s policy discourse articulate the relationship between Great Power status and China’s responsibility to help address climate change?}}

\author{\textit{2270405d}}

 % Date, use \date{} for no date
\date{\today}

% save these variables for later use under different name
\makeatletter
\let\doctitle\@title
\let\docauthor\@author
\let\docdate\@date
\makeatother

%----------------------------------------------------------------------------------------
%	HEADER AND FOOTER SECTION
%----------------------------------------------------------------------------------------
\pagestyle{fancy}

% sets both header and footer to nothing
\fancyhf{}

% Header
% remove horizontal header bar
\renewcommand{\headrulewidth}{0pt}
% left hand side header
\lhead{\textbf{China's International Relations Essay}}
% right hand side header
\rhead{\docauthor}


% Footer
% center of footer
\cfoot{\thepage\ of \pageref{LastPage}}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title section

%----------------------------------------------------------------------------------------
%	ABSTRACT AND KEYWORDS
%----------------------------------------------------------------------------------------

% Uncomment to change the name of the abstract to something else
%\renewcommand{\abstractname}{Summary}

%\begin{abstract}
%	abstract
%\end{abstract}

% Keywords
\hspace*{3.6mm}\textit{Keywords:} China, policy discourse, climate change, great power, responsibility

% Vertical whitespace between the abstract and first section
\vspace{30pt}

%----------------------------------------------------------------------------------------
%	ESSAY BODY
%----------------------------------------------------------------------------------------

As the United States (US) is withdrawing from the COP21 Paris agreements and the PRC has committed to binding reductions in greenhouse gas emissions, this essay will explore how Chinese policy discourse articulates Great Power status and responsibility to address Climate Change. After defining "policy as discourse", this essay will explain the gap between Chinese environmental policy and its implementation. It will address the notion of a Great Power and ensuing "responsibilities", underlining that China has ambiguously accepted the status of Great Power as it points to "Western" greenhouse gas (GHG) emissions as the primary cause of Global Warming. Finally this essay will define the emerging \textit{New Normal} role of China in Climate Change mitigation as it grew to become one of the most prominent actors in the fight against Climate Change.

\section*{Policy as discourse}

\textit{Policy as discourse} can be said to be \textit{discourse through policy} because of the tendency for policy to construct the problems it addresses by setting the boundaries of a problem and its solutions simultaneously \autocite{bacchi_policy_2000}. This is especially true of the PRC where environmental governance is conducted through a model of "authoritarian environmentalism" \autocite{gilley_authoritarian_2012}. Indeed, the "non-participatory" model of Chinese environmental policymaking reinforces this effect of \textit{Policy as discourse} whereby environmental policymaking is highly centralized and hence, capable of rapidly producing legislation  \autocite{gilley_authoritarian_2012,mol_chinas_2006,ran_perverse_2013,richerzhagen_chinas_2008}.

\section*{Policy and implementation gap}

There exists a substantial gap between the centrally defined national environmental policy and its decentralized local implementations. This gap is due to the perverse incentives defined at the national level with sometimes competing incentives (e.g. for job and wealth creation) are higher than those for implementing environmental regulations \autocite{gilley_authoritarian_2012,mol_chinas_2006}. Market incentives and fines to Chinese companies also suffer from the same problem, it is often more profitable to pay the fine for the pollution than to mitigate it beforehand (e.g. sulphure discharge charges) \autocite{mol_chinas_2006}. The objectives of companies and local officials are thus often aligned (identical in the case of State Owned Enterprises) which leads to high levels of corruption \autocite{mol_chinas_2006}. While it seems like the cause of these problems is a poorly implemented policy, the issue actually resides in a conflict between environmental and economic regulations, the latter of which is made predominant by the national legislation, not by incompetence, by choice \autocite{ran_perverse_2013}.

However, the time scale of Global Warming is such that the urgency of the issue is hardly comprehended by most of us as individuals, the Chinese public is no exception to this \autocite{gilley_authoritarian_2012,mol_chinas_2006,ran_perverse_2013,richerzhagen_chinas_2008}. Chinese public environmental concern will be focused on local issues and since meaningful democracy happens at the local level, environmental issues supported by individuals are the most likely to be followed through due to devolution and accountability at local levels \autocite{michaelowa_feeling_2012}. Chinese public issue perception cites air and water quality as more pressing environmental issues than Global Warming and this echoes a broader tendency for the Chinese government to frame Global Warming as an "expert domain" \autocite{gilley_authoritarian_2012}. This is partly out of genuine scientific complexity but also to keep ascertaining control over environmental policy as well as its pace of implementation \autocite{gilley_authoritarian_2012,michaelowa_feeling_2012,wubbeke_science-politics_2013}. Informal norms and social networks of influence are thus too weak to significantly influence the implementation of policy mitigating global warming.

\section*{Great Power status and responsibility}

What is a Great Power? Mainstream International Relations (IR) theory often seeks an answer in military and economic metrics \autocite{deng_china_2004,kim_china_1997}. There exists a more nuanced answer, as advocated by the English School (ES), which instead portrays Great Powers as a "club" whose membership defines status as a Great Power \autocite{kopra_great_2016,ringmar_recognition_2002,suzuki_seeking_2008}. Great Powers are recognized as such by existing Great Powers, thus, some have argued that the PRC is a "frustrated great power" \autocite{bijian_chinas_2005} playing a "recognition game" \autocite{ringmar_recognition_2002} within the international community. China seeks status "within the normative framework of international society" \autocite[60]{suzuki_seeking_2008} by cultivating the image of a "responsible great power" \autocite[194]{xi_xi_2014} which, some have argued, it is being denied. When it comes to Climate Change, Chinese discourse has instrumented and cultivated this ambiguity around the PRC's status as a Great Power. In Xi Jinping's words "We are keenly aware that China will remain the world’s largest developing country for a long time and that to improve life for its 1.3 billion people calls for strenuous efforts." \autocite[292]{xi_xi_2014}, this is the same book in which he calls China "a responsible great power" \autocite[194]{xi_xi_2014}.

American administrations have successively called for China to become a "responsible stakeholder" of the international community \autocite{etzioni_is_2011,kopra_great_2016}. This is echoed in statements such as Robert Zoellick's, then Deputy Secretary of State for the Bush Administration: "As a responsible stakeholder, China would be more than just a member - it would work with us to sustain the international system that has enabled its success" \autocite{robert_zoellick_whither_2005}. Chinese policy defines the PRC's status whithin the international community differently, it does not claim or acknowledge "Great Power responsibility", it pushes for a further multi-polar world, Hu Jintao's "harmonious society" \autocite{johnson_hu_2011}. In fact, the Chinese response to \textcite{robert_zoellick_whither_2005}'s statement cunningly states that the developed world should shoulder greater responsibility for the development of the international community without categorizing China as as developed nation \autocite{kopra_great_2016}.

\section*{Common but differentiated responsibility}

The discourse of China as a "developing nation" is that for "the most populous developing country, to run itself well is is the most important fulfilment of its international responsibility" \autocite{information_office_of_the_state_council_of_the_peoples_republic_of_china_chinas_2011}. Furthermore, the PRC advocates this narrative of development \textit{before} Climate Change mitigation for all developing countries, underlining their right to development according to the principle of \textit{common but differentiated responsibilities} \autocite{gong_what_2011,kopra_great_2016} whereby “that global responsibility depends on a state’s development stage rather than its global impacts” \autocite[29]{kopra_great_2016}. This is echoed in official policy on the matter, which notes that China “\textit{should} [...] pursue green and low-carbon energy development \textit{suited to our national conditions}” \autocite[144]{xi_xi_2014} (emphasis added). This insistence on China’s national condition seems to apply economically and politically, articulating a justification that Chinese efforts to combat climate change be relative to its developing political system and its economy \autocite{harris_chinas_2011}.

From an economic perspective, Chinese international policy discourse on global warming highlights the fact that a considerable portion of China's greenhouse gas (GHG) emissions are from "western" goods manufacturing \autocite{buckley_reuters_2008}. This originates from Chinese research which has shown that between 10 to 27\% of Chinese CO\textsubscript{2} emissions are from Chinese goods manufactured for export to "western countries" \autocite[356]{yunfeng_chinas_2010}.

\section*{Historic Responsibility}

Chinese discourse on climate change also mentions the principle of "historic responsibility" \autocite{xi_work_2015}. If it is one, China is a very recent great power, as such, although its CO\textsubscript{2} emissions are considerable, they are quite recent. In fact, as of 2009, the US accounts for 48\% of cumulative CO\textsubscript{2} emissions since 1900, 24\% for "Western Europe" and 16\% for China \autocite[572]{botzen_cumulative_2008}. Furthermore, Chinese literature (supported elsewhere \textcite{spracklen_global_2016}) supporting the policy shows that this "historic responsibility" is particularly important in the case of Global Warming. Indeed, the life spans of different gases in the atmosphere vary, furthermore they contribute differently to Radiative Forcing which causes Global Warming \autocite{shindell_radiative_2013}. Since GHG have long atmospheric lifespans, Chinese GHG emissions are larger than their contribution to Radiative Forcing since it started emitting them later than other countries \autocite{ding_control_2009,li_contribution_2016}. The Chinese scientific community supporting the policy thus highlights that since cumulative Chinese CO\textsubscript{2} emissions are lower than "developed countries'", the Chinese contribution to Global Warming is also considerably less , 10\% of total radiative forcing \autocite[361]{li_contribution_2016}.

\section*{Future responsibility}

Until the 2016 COP21 Paris Climate Agreements, the PRC had systematically refused any binding GHG emissions limitations at the international (Copenhagen COP15, Kyoto Protocol) or at the national level (Five Year Plans) \autocite{gong_what_2011,harris_chinas_2011,hilton_paris_2017}. This is despite the fact that since 2006 the PRC has been the largest CO\textsubscript{2} emitter the Chinese response has been to reframe the figures of CO\textsubscript{2} emissions to a per-capita metric, where it is far outranked by many "developed" nations \autocite{gong_what_2011,harris_chinas_2011}. However, projections note that China is on course to overtake Europe in terms of per-capita emissions by 2030 \autocite{harris_chinas_2011}.

In fact the non-binding goals for the PRC after the 1992 United Nations framework Convention on Climate Change (UNFCCC) were a reformulation of pre-existing energy efficiency objectives from the 11th Five Year Plan as measures to combat Global Warming \autocite{harris_chinas_2011,richerzhagen_chinas_2008}. Some have said that such objectives were formulated primarily in terms of GDP growth and energy security rather than climate change mitigation \autocite{harris_chinas_2011,michaelowa_feeling_2012}. Indeed, Xi Jinping has underlined this importance for energy independence stating that “While relying mainly on domestic energy sources, we will strengthen international cooperation in all sectors related to energy production and consumption, and make effective use of resources from other countries” \autocite[144]{xi_xi_2014}. This instrumentation of Global Warming mitigation has also been applied to Chinese trade policy, whereby VAT tax reform was aimed at reducing energy-intensive exports to improve energy independence and export tax reform aimed at favouring domestic production \autocite{eisenbarth_is_2017}.

However, climate change research is demonstrating that, in Xi Jinping's own words, “We must recognize the fact that China is still an ecologically vulnerable country” \autocite[229]{xi_xi_2014} \autocite{gong_what_2011,michaelowa_feeling_2012}. This has led to a shift in Chinese policy on Global Warming to its current discourse. Today the PRC still abides by the principle of "common but differentiated responsibility" noting that developed nations' emissions are still responsible for Climate Change according to the principle of "cumulative per capita emissions" \autocite{ding_control_2009}. However, the 12th Five Year Plan and the Paris COP21 agreements have marked a dramatic shift in economic and environmental policy discourse that has been called China's "New Normal" role in international climate negotiations \autocite{green_chinas_2017,hilton_paris_2017,kopra_great_2016}.

\section*{China's \textit{New Normal} role in Global Warming mitigation}

A key initiator of this change has been knowledge transfer and climate research, while climate research has been present in China, it has suffered from a lack of influence amongst the political circles \autocite{wubbeke_science-politics_2013}. However, this is changing, notably with technology developments (notably in renewable energy sources) as well as transfers from international scientific cooperation, in part due to the Chinese Communist Party's (CCP) decision to transition the Chinese economy away from labour based economic growth as part of its 12th and 13th Five Year Plan \autocite{green_chinas_2017,michaelowa_feeling_2012}.

Perhaps this is where Chinese \textit{environmental authoritarianism} shines, top-down ambitious central policy planning has set the nation on course to peak CO\textsubscript{2} emissions by 2030 \autocite{hilton_paris_2017,kopra_great_2016,qin_climate_2016}. In fact, the PRC's commitment was set to under-promise and over-deliver as estimates point to a peak in CO\textsubscript{2} emissions "at some point in the decade before 2025" \autocite[14]{green_chinas_2017}

In fact part of the reason for the inclusion for binding reductions in GHG emissions in the COP21 Paris agreements is due to this push up the value chain for the Chinese economy. Indeed, climate research has made the CCP leadership aware that “Protecting the environment equates to protecting productivity and that improving the environment also equates to developing productivity.” \autocite[231]{xi_xi_2014} a considerable change from the policy opposition between GDP growth and climate change, a gap today filled by the PRCs ambitious economic transition \autocite{green_chinas_2017,michaelowa_feeling_2012}. This opening-up of climate-change research is one of the manifestations of the values of "win-win", "fair" and "inclusive" cooperation \autocite{xi_work_2015} advocated in Chinese policy discourse on Global Warming, and also more generally in China's foreign policy. The PRC is arguing against the debatably western notion of "Great Power responsibility" \autocite{kopra_great_2016} by promoting multilateralism rather than polarisation of Great Power relations, in a challenge to American hegemony in the international community \autocite{deng_china_2004}. With the US withdrawal from the Paris COP21 agreements as well as his questioning of US presence in Syria and of the North Atlantic Treaty Organization (NATO), Donald Trump himself has encouraged the new \textit{Chinese way forward} of "multilateralism" and "cooperation" not only for Climate Change mitigation, but for International Relations as a whole.

\section*{Conclusion}

The Chinese policy discourse has often been ambiguous about China's status as a "Great Power". On the one hand, Chinese environmental legislation is centrally defined according to a model of "authoritarian environmentalism" which can be effective due to its top-down authoritarian nature. On the other hand, devolution in China has given much environmental executive power to local governments with opposing incentives that often encourage the avoidance of environmental legislation. Furthermore, there exists a notable ammount of corruption amongst local governments hindering policy implementation efforts as well as a poorly informed population whose sole political power is at local level and is thus used to promote regional and visible environmental issues. This is echoed at the international level, where China has hesitantly endorsed the role of a Great Power during climate negotiations and refusing binding GHG emissions while acknowledging its status in other domains. This is also perhaps due to the "club like" nature of Great Powers, which, according to English School IR theory, can recognize another "Great Power".

On the other hand, Chinese policy on Global Warming and its role in it has shifted from the rhetoric of primarily western responsibility to also recognising its own rising role, its interest in mitigating the issue. Originally, the PRC refused binding GHG emissions, opposing them to economic growth and unwilling to compromise the latter. This policy then shifted, the Chinese discourse still pointed to "Western" historical and population-relative emissions as the primary causes of Global Warming while attempting to re-council its economic growth with emissions reductions through central policies for energy conservation, but aimed mainly as energy independence. Chinese policy discourse as a whole rejects the idea of "Great Power responsibility", answering American calls to behave as a "responsible global power" by highlighting the western role in Global Warming. Indeed the PRC has underlined its right and "responsibility" to development as the most populous developing nation. In addition to underlining the share of Chinese GHG emissions related to "Western" goods manufacturing, the PRC's discourse notes the \textit{historical responsibility} for the presence of GHG with long lifespans in the atmosphere. Summarising this in a per-capita historical measure of GHG emissions, Chinese research has noted that the PRC has been responsible for solely 10\% of radiative forcing. Today, China's ambitious economic transition is reflected in its \textit{New Normal Role} in Climate Change mitigation. As it sets out to start decreasing its GHG emissions by 2030 and as the US have withdrawn from the Paris COP21 agreements, the PRC might reject the notion of "Great Power Responsibility" in the face of Global Warming but it has certainly opened an avenue fulfilling it.


\newpage
%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY
%----------------------------------------------------------------------------------------

\emergencystretch=1em

\printbibliography

%----------------------------------------------------------------------------------------

\end{document}
